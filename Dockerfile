#Dockerfile para criar um container com nginx e reactjs


FROM node:16 as builder

WORKDIR /app/frontend

COPY package* ./

COPY . .

RUN npm install && npm run build

FROM nginx

WORKDIR /usr/share/nginx/html

COPY --from=builder  /app/frontend/build /usr/share/nginx/html

EXPOSE 80

